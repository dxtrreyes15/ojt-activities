CREATE DATABASE airline_system;
USE airline_system;

CREATE TABLE User_Account( -- USER_ACCOUNT TABLE
	User_id int NOT NULL AUTO_INCREMENT,
	Username varchar(50) NOT NULL,
    Password varchar(50) NOT NULL,
    email varchar(50) NOT NULL,
    is_superuser bool NOT NULL,
    is_staff bool NOT NULL,
    is_active bool NOT NULL,
    date_joince datetime NOT NULL,
    Last_login datetime NOT NULL,
    PRIMARY KEY(User_id));

-- CREATE VIEW user_acc as SELECT * FROM User_Account;
-- SELECT * FROM user_acc;
-- DROP VIEW user_acc;

SELECT * FROM User_Account;

CREATE TABLE Customer_info(
	User_id int,
	Customer_id int NOT NULL AUTO_INCREMENT,
	first_name varchar(50) NOT NULL,
    last_name varchar(50) NOT NULL, 
    email varchar(50) NOT NULL,
    contact_number int NOT NULL,
    Passport_numbeer int NOT NULL,
    passport_expiration_date date NOT NULL,
    PRIMARY KEY(Customer_id),
    FOREIGN KEY (User_id) REFERENCES User_Account(User_id));
    
SELECT * FROM Customer_info;

CREATE TABLE Flight_transaction(
	transaction_id int NOT NULL,
    Customer_id int NOT NULL,
    full_name varchar(120) NOT NULL,
    email varchar(50) NOT NULL,
    contact_number int NOT NULL,
    cabin_type varchar(100) NOT NULL,
    luggage_weight int NOT NULL,
    quantity int NOT NULL,
    flight_code varchar(50) NOT NULL,
    airline varchar(100) NOT NULL,
    payment_status bool NOT NULL,
    PRIMARY KEY(transaction_id)); -- FOREIGN KEY (Customer_id) REFERENCES Customer_info(Customer_id)
    
ALTER TABLE Flight_transaction ADD FOREIGN KEY (Customer_id) REFERENCES Customer_info(Customer_id);
ALTER TABLE Flight_transaction MODIFY transaction_id int NOT NULL AUTO_INCREMENT;
    
SELECT * FROM Flight_transaction;

CREATE TABLE Airline_info(
	Airline_id int NOT NULL AUTO_INCREMENT,
    airline_name varchar(120) NOT NULL,
    airline_info varchar(200),
    PRIMARY KEY(Airline_id));

SELECT * FROM Airline_info;

CREATE TABLE airplane(
	plane_code int NOT NULL AUTO_INCREMENT,
    airline_company varchar(120) NOT NULL,
	Airline_id int,
    PRIMARY KEY(plane_code),
	FOREIGN KEY (Airline_id) REFERENCES Airline_info(Airline_id));
    
SELECT * FROM airplane;

CREATE TABLE Flight(
	Flight_code varchar(50) NOT NULL,
    Airline_company varchar(120) NOT NULL, -- QUESTION: relationship to flight??
    created_by varchar(100) NOT NULL,
	terminal int NOT NULL, 
	remarks varchar(100) NOT NULL,
    origin varchar(100) NOT NULL,
    gate int NOT NULL,
    destination varchar(100) NOT NULL,
    plane_code int,
    plane_availability varchar(50) NOT NULL,
    Price int NOT NULL,
    PRIMARY KEY(Flight_code),
    FOREIGN KEY (plane_code) REFERENCES airplane(plane_code));
    
SELECT * FROM Flight;

CREATE TABLE Flight_information(
	flight_info_id int NOT NULL AUTO_INCREMENT,
    Flight_code varchar(50),
    arrival_time time NOT NULL,
    departure_time time NOT NULL,
    A_D_status varchar(50) NOT NULL,
    flight_status varchar(50) NOT NULL,
    delayed_time time NOT NULL,
    PRIMARY KEY(flight_info_id),
    FOREIGN KEY (Flight_code) REFERENCES Flight(Flight_code));

SELECT * FROM Flight_information;

CREATE TABLE Ticket_info(
	Ticket_id int NOT NULL AUTO_INCREMENT,
    transaction_id int,
    Customer_id int,
    flight_info_id int,
    flight_code varchar(50),
    full_name varchar(100) NOT NULL,
    origin varchar(100) NOT NULL,
    destination varchar(100) NOT NULL,
    seat_position varchar(50) NOT NULL,
    departure_time time NOT NULL,
    luggage_weight int NOT NULL,
    PRIMARY KEY(Ticket_id),
    FOREIGN KEY (transaction_id) REFERENCES Flight_transaction(transaction_id),
    FOREIGN KEY (Customer_id) REFERENCES Flight_transaction(Customer_id),
    FOREIGN KEY (flight_info_id) REFERENCES Flight_information(flight_info_id),
    FOREIGN KEY (flight_code) REFERENCES Flight(Flight_code));

SELECT * FROM Ticket_info;

