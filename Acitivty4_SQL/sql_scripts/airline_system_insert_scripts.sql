-- insert 2 data from the flight table with the following condition:
-- the flight_code must be 101 and 102

INSERT INTO flight(Flight_code, Airline_company, created_by, terminal,
remarks, origin, gate, destination, plane_code, plane_availability, Price)
VALUES ('101', "Cebu Pacific", "dexter", 2, "test_remarks", "Manila",  5, "Bohol", 2, "Available", 500);

INSERT INTO flight
VALUES ('102', "Air Asia", "cardo", 1, "test_remarks1", "Manila",  4, "Bohol", 1,  "Available", 600);

INSERT INTO flight
VALUES ('111', "Air Asia", "jun", 1, "test_remarks2", "Manila",  4, "Cebu", 2,  "Available", 800);

SELECT * FROM flight;

-- insert 10 data from the flight_transaction with the following conditions:
-- the cabin_type must be 4 economy,3 premium economy, 2 business class , 1 first class
INSERT INTO flight_transaction(Customer_id, full_name, email, contact_number, cabin_type,
	luggage_weight, quantity, flight_code, airline, payment_status)
VALUES
	(1, "Jojo Gutierrez", "jojoboy@gmail.com", 912333, "economy", 5, 1, "101", "Cebu Pacific", 1),
	(2, "Jojo Gutierrez", "jojoboy@gmail.com", 912333, "economy", 3, 1, "102", "Air Asia", 1),
    (1, "Jojo Gutierrez", "jojoboy@gmail.com", 912333, "economy", 4, 1, "103", "Cebu Pacific", 1),
    (3, "Ivy Marie Gutierrez", "vmay@gmail.com", 92343, "economy", 3, 1, "104", "Pan Pacific", 1),
    (4, "Ivy Marie Gutierrez", "vmay@gmail.com", 92343, "premium economy", 2, 1, "105", "Pan Pacific", 1),
    (3, "Ivy Marie Gutierrez", "vmay@gmail.com", 92343, "premium economy", 3, 1, "106", "Air Asia", 0),
    (5, "Nicolei Gutierrez", "nicolei@gmail.com", 94789, "premiuum economy", 3, 1, "107", "Air Asia", 1),
    (1, "Jojo Gutierrez", "jojoboy@gmail.com", 912333, "business class", 2, 1, "108", "Phil Airlines", 0),
	(4, "Ivy Marie Gutierrez", "vmay@gmail.com", 92343, "business class", 3, 1, "109", "Phil Airlines", 1),
    (5, "Nicolei Gutierrez", "nicolei@gmail.com", 94789, "first class", 3, 1, "110", "Cebu Pacific", 1);
    
SELECT * FROM flight_transaction;

-- insert 10 data from the ticket_info with the following conditions:
-- the destination must be 3 cebu, 7 bohol
-- the origin must be manila only
INSERT INTO ticket_info(transaction_id, Customer_id, flight_info_id, flight_code, full_name, origin,
	destination, seat_position, departure_time, luggage_weight)
VALUES
	(1, 1, 1, "101", "Jojo Gutierrez", "Manila", "Bohol", "2B", '10:00:00', 5),
    (2, 2, 2, "102", "Jojo Gutierrez", "Manila", "Bohol", "12C", '12:30:00', 3),
    (3, 1, 3, "103", "Jojo Gutierrez", "Manila", "Cebu", "1A", '10:00:00', 4),
    (4, 3, 4, "104", "Ivy Marie Gutierrez", "Manila", "Bohol", "14A", '12:00:00', 3),
    (5, 4, 5, "105", "Ivy Marie Gutierrez", "Manila", "Cebu", "20A", '01:00:00', 2),
    (6, 3, 6, "106", "Ivy Marie Gutierrez", "Manila", "Cebu", "3C", '02:00:00', 3),
    (7, 5, 7, "107", "Nicolei Gutierrez", "Manila", "Bohol", "5A", '04:00:00', 3),
    (8, 1, 8, "108", "Jojo Gutierrez", "Manila", "Bohol", "13B", '03:30:00', 2),
    (9, 4, 9, "109", "Ivy Marie Gutierrez", "Manila", "Bohol", "9C", '06:30:00', 3),
    (10, 5, 10, "110", "Nicolei Gutierrez", "Manila", "Bohol", "2A", '07:30:00', 3);


	
    
    
