-- count all the tickets by its cabin_type

SELECT COUNT(DISTINCT(CASE WHEN a.cabin_type='economy' THEN a.transaction_id END)) AS economy,
COUNT(DISTINCT(CASE WHEN a.cabin_type='premium economy' THEN a.transaction_id END)) AS premium_economy ,
COUNT(DISTINCT(CASE WHEN a.cabin_type='business class' THEN a.transaction_id END)) AS business_class ,
COUNT(DISTINCT(CASE WHEN a.cabin_type='first class' THEN a.transaction_id END)) AS first_class  FROM ticket_info b
JOIN flight_transaction a
ON b.transaction_id = a.transaction_id;

-- list all the data from the ticket_info with the flight_code 101

SELECT ticket_info.* FROM ticket_info
LEFT JOIN flight ON ticket_info.flight_code = flight.Flight_code
WHERE flight.Flight_code="101";

-- list all the data from the ticket_info with the cabin_type premium economy
SELECT ticket_info.* FROM ticket_info
LEFT JOIN flight_transaction ON ticket_info.transaction_id = flight_transaction.transaction_id
WHERE flight_transaction.cabin_type="premium economy";

-- 1.create a query that list all the data from the ticket_info
-- 2.create a query that list all the data from the ticket_info with destination to bohol
-- 3. create a query that list all the data from the ticket_info with destination to davao

SELECT * FROM ticket_info;
SELECT * FROM ticket_info WHERE destination="Bohol";
SELECT * FROM ticket_info WHERE destination="Davao";


