from datetime import datetime as dt

# create date objects
date1 = dt(2014, 7, 2)
date2 = dt(2014, 7, 11)
  
print(date1, date2)
# get date diff
diff = date2 - date1 # returns datetime
print(f"{diff.days} days")