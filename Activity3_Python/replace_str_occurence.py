from dataclasses import replace


sample_str = "sasss" 

# using one-liner if-else and list comprehension
print(sample_str[0] + "".join(["$" if char == sample_str[0] else char \
    for char in sample_str[1:]]))
#  r+ ['e', 's', 't', 'a', '$', 't']

# other sol
# char = sample_str[0]
# replaced = char + sample_str[1:].replace(char, "$")
# print(replaced)


