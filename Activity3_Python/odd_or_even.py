number = ""
while True:
    try:
        number = int(input("Enter a number: ")) #cast
    except ValueError: #catch non-number
        print("Invalid input.")
    else:
        print("number is even" if (number % 2) == 0 else "number is odd")
        break
