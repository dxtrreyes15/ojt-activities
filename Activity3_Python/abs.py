def within(number):
    return(abs(1000 - number) <= 100 or abs(2000 - number) <= 100) 

print(within(900))
print(within(1100))
print(within(800))
print(within(1800))