from full_name import Person

class Woman(Person): #child class
    def __init__(self, first_name, middle_name, last_name):
        Person.__init__(self, first_name, middle_name, last_name) #without super function

    def get_full_name(self): #override
        return Person(self.first_name, self.middle_name, self.last_name).get_full_name().upper() #without super function

p1 = Woman("Jaja", "Conag", "Cruz")
print(p1.get_full_name())