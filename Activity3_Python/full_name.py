class Person: #class declaration
    def __init__(self, first_name, middle_name, last_name):
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name

    def get_full_name(self):
        return f"{self.first_name} {self.middle_name} {self.last_name}"

person1 = Person("carding", "probinsyano", "dalisay") #instansiate
# print(person1.get_full_name())