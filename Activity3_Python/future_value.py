#Future value formula: FV=PV*(1+r)^n

principal_value = 10000 #Principal Value
interest_rate = 3.5 * 0.01 #rate of interest in decimal form
years = 7 #number of years

future_value = principal_value*((1+interest_rate) ** years) #Future Value
print(round(future_value,2)) #2 decimal places