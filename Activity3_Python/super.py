class Employee:
    def __init__(self, name, age):
        self.name = name
        self.age = age

class HR(Employee):
    def __init__(self, name, age, department):
        super().__init__(name, age) #inherit parent props
        self.department = department

    def printEmployee(self):
        print(self.name, self.age, self.department)

hr1 = HR("Monica Gastambide", 25, "DeptHR .")
hr1.printEmployee()

# other class
class Student:
    def __init__(self, stud_id, name, age, course):
        self.stud_id = stud_id
        self.name = name
        self.age = age
        self.course = course
    
    def print_student_info(self):
        return f"{self.stud_id} {self.name} {self.age} {self.course}"

class CollegeStudent(Student):
    def __init__(self, stud_id, name, age, course, subjects):
        super().__init__(stud_id, name, age, course) # without using the parent class's name
        self.subjects = subjects

    def printSubjects(self):
        return f"{super().print_student_info()} {self.subjects}" 

stud1 = CollegeStudent("1", "Dexter", 24, "BSIT", {"A", "B"})
print(stud1.printSubjects())
