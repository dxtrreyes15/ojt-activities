import datetime as dt

def same_type(f_param, s_param):
    if isinstance(f_param, str) and isinstance(s_param, str):
        return f_param + s_param
    elif isinstance(f_param, int) and isinstance(s_param, int):
        return f_param * s_param
    elif isinstance(f_param, list) and isinstance(s_param, list):
        return f_param + s_param
    elif isinstance(f_param, dict) and isinstance(s_param, dict):
        new_dict = f_param | s_param #union
        return new_dict
    elif isinstance(f_param, dt.datetime) and isinstance(s_param, dt.datetime):
        time = f_param.time() #get time
        time_delta = dt.timedelta(hours=time.hour, minutes=time.minute, seconds=time.second) #create timedelta - time to be added to other datetime
        date_sum = s_param + time_delta # add timedelta to datetime
        return date_sum

        # Sir Angel's Solution
        # time_obj = dt.datetime.strptime('0:00:00', '%H:%M:%S')
        # date_sum = ((f_param - time_obj) + s_param).time() # add datetime.datetime with timedelta object
        # return date_sum
    else:
        print("Not of same type")

print(same_type("I am ","Groot!"))
print(same_type(5,5))
print(same_type([1,2,4], ["a","b","c"]))
print(same_type({"test1": 25, "test2": 50}, {"test3": 25}))
print(same_type({"test1": 25, "test2": 50}, 5))

a_time = dt.datetime.strptime('2022 05 15 20:00:00', '%Y %m %d %H:%M:%S')
b_time = dt.datetime.strptime('2022 05 15 21:00:00', '%Y %m %d %H:%M:%S')
# adding the datetime above should result to 2022-05-16 17:00:00

# a_time = dt.datetime.now()
# b_time = dt.datetime.now() + dt.timedelta(hours=4, minutes=12, seconds=20)
# print(dt.datetime.strptime('2022 05 15 20:00:00', '%Y %m %d %H:%M:%S'))

res = same_type(a_time ,b_time)
print(res)
