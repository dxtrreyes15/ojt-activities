$(document).ready(function() {
    // add a slidedown animation from the top after 2 seconds when the page loads.
    console.log("Test JQUERY")

    var list_of_post = []

    $('#login_page_container').hide()    
    $('#login_page_container').slideDown(2000)

    // change border of input elements to green when user type on it
    // $('#username').keypress(function(event) {
    //    $(this).addClass('border border-success')
    // })

    // $('#password').keypress(function(event) {
    //     $(this).addClass('border border-success')
    //  })

    $('input').keypress(function() {
        $(this).addClass('border border-success')
    })

     // add 'form-control-sm' class in all input
    $('input').addClass('form-control-sm')

    // disable a button when clicked
    // add a loader when clicked
    $('#login_btn').on('click', function() {
        $(this).prop('disabled', true) // add disabled prop - set to true
        $('#login_text').hide() // hide login text
        $('#spinner').addClass('d-inline-block') // display the spinned
    })

    // add an option where user can change the background color
    $('#select_bg_color').on('change', function() {
        var selected_color = $(this).val() // get current selected value
        $('body').css('backgroundColor', selected_color) // set the background color
    })

    // add a lead class on all p elements
    $('p').addClass('lead')

    // display an image in a large modal when clicked
    $('._imgs').on('click', function() {
        var img_path = $(this).attr('src') // get relative path
        // console.log(img_path)
        $('#image_lg_modal').find('#modal_actual_img').prop('src', img_path) // set img
        $('#image_lg_modal').modal('toggle') // toggle the modal
    })
    // add and show an h1 element with a text that comes from an external file
    $('#login_btn').on('click', function() {
        var head = $('<h2>')
        head.load('title.txt')
        
        $('#welcome_title').append(head)
    })

    // fetch data from api
    $.get('https://jsonplaceholder.typicode.com/posts', function(data, status) {
    
        if(status == 'success') { // check status
            list_of_post = data
            console.log(data.length)
            data.forEach(post => {
                console.log(post.title)
                // data container structure
                var row = $('<tr>')
                var row_data = $(`
                <td>${post.id}</td>
                <td>${post.userId}</td>
                <td>${post.title}</td>
                <td>${post.body}</td>
                `)
                
                row.append(row_data)
                // append
                $('#post_tbl_body').append(row)
            })
        } else {
            alert('Something went wrong.')
        }
    })

    // append new post
    function updateView(post_data) {
        // var recent_post = list_of_post[list_of_post.length -1]
            var row = $('<tr>')
            var row_data = $(`
            <td>${post_data.id}</td>
            <td>${post_data.userId}</td>
            <td>${post_data.title}</td>
            <td>${post_data.body}</td>
            `)

            row.append(row_data)
            // append
            $('#post_tbl_body').append(row)
    }

    $('#post_form').on('submit', async function(e) {
        e.preventDefault() // preempt default form action
         
        if($('#user_id').val() != "" && $('#post_title').val() != "" && $('#post_body').val() !="") {
            var last_count = await $.get('https://jsonplaceholder.typicode.com/posts') // last post count
            var id = last_count.length+ 1
            var userId = $('#user_id').val()
            var post_title = $('#post_title').val()
            var post_body = $('#post_body').val()

            var post_obj = {id: id, userId: userId, title: post_title, body: post_body} // post data

            $.ajax({ // ajax POST request
                type: 'POST',
                url: 'https://jsonplaceholer.typicode.com/posts',
                data: post_obj,
                success: function(response) { // will fire on success
                    list_of_post.push(post_obj)
                    /* empty-out input fields */
                    $('#user_id').val("")
                    $('#post_title').val("")
                    $('#post_body').val("")
                    /* update posts list */
                    updateView(response)
                    alert('Successfully added a post.')
                },
                error: function() {
                    alert('ERROR HAS OCCURED')
                }
            })
        } else {
            alert("Something went wrong")
        }
        
    })

    // document ready
})